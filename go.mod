module gitlab.com/krismolendyke/lblval

go 1.13

require (
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/google/gofuzz v1.1.0 // indirect
	golang.org/x/net v0.0.0-20200114155413-6afb5195e5aa // indirect
	k8s.io/api v0.17.2
	k8s.io/apimachinery v0.17.2
)
