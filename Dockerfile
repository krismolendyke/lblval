# docker build -t localhost:32000/lblval .

FROM golang:1.13-buster as builder

WORKDIR /go/src/app
ADD go.mod /go/src/app
ADD go.sum /go/src/app
RUN go mod download

ADD . /go/src/app
RUN go build -o /go/bin/app ./cmd/lblval-server


FROM gcr.io/distroless/base:nonroot

COPY --from=builder /go/bin/app /

ENTRYPOINT ["/app"]
