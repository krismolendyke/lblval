package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"encoding/json"

	"strings"

	admissionv1 "k8s.io/api/admission/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

func TestMain(m *testing.M) {
	log.SetOutput(ioutil.Discard)
	os.Exit(m.Run())
}

func TestValidateLabel(t *testing.T) {
	tests := map[string]struct {
		rawObject     string
		requiredLabel label
		allowed       bool
	}{
		"Disallowed: invalid object":                                {rawObject: `{`, allowed: false},
		"Disallowed: empty object":                                  {rawObject: `{}`, allowed: false},
		"Disallowed: object incorrect key":                          {rawObject: `{"metadata": {"labels": {"k1": "v0"}}}`, requiredLabel: label{"k0", "v0"}, allowed: false},
		"Disallowed: object incorrect value":                        {rawObject: `{"metadata": {"labels": {"k0": "v1"}}}`, requiredLabel: label{"k0", "v0"}, allowed: false},
		"Disallowed: missing required label":                        {rawObject: `{"metadata": {"labels": {"k0": "v0"}}}`, allowed: false},
		"Disallowed: missing object labels, no required label":      {rawObject: `{"metadata": {}}`, allowed: false},
		"Disallowed: empty object labels, no required label":        {rawObject: `{"metadata": {"labels": {}}}`, allowed: false},
		"Disallowed: empty label value, empty required label value": {rawObject: `{"metadata": {"labels": {"k0": ""}}}`, requiredLabel: label{"k0", ""}, allowed: false},
		"Allowed: object label is required label":                   {rawObject: `{"metadata": {"labels": {"k0": "v0"}}}`, requiredLabel: label{"k0", "v0"}, allowed: true},
		"Allowed: object labels contain required label":             {rawObject: `{"metadata": {"labels": {"k0": "v0", "k1": "v0"}}}`, requiredLabel: label{"k0", "v0"}, allowed: true},
	}
	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			ar := admissionv1.AdmissionReview{
				Request: &admissionv1.AdmissionRequest{
					Object: runtime.RawExtension{
						Raw: []byte(tc.rawObject),
					},
				},
			}
			if got := validateLabel(ar, tc.requiredLabel); got.Allowed != tc.allowed {
				t.Errorf("got %v, want %v: %s", got.Allowed, tc.allowed, got.Result.Message)
			}
		})
	}
}

func TestLabel(t *testing.T) {
	tests := map[string]struct {
		label label
		want  bool
	}{
		"Invalid: empty key & value": {label{}, false},
		"Invalid: empty key":         {label{"", "v"}, false},
		"Invalid: empty value":       {label{"k", ""}, false},
		"Valid":                      {label{"k", "v"}, true},
	}
	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			if got := tc.label.Valid(); got != tc.want {
				t.Errorf("got: %v, want: %v", got, tc.want)
			}
		})
	}
}

func TestHTTPServer(t *testing.T) {
	tests := map[string]struct {
		requiredLabel label
		rawObject     string
		allowed       bool
	}{
		"Disallowed: empty object":                     {requiredLabel: label{"k0", "v0"}, rawObject: `{}`, allowed: false},
		"Disallowed: object missing required label":    {requiredLabel: label{"k0", "v0"}, rawObject: `{"metadata": {}}`, allowed: false},
		"Disallowed: object label has different value": {requiredLabel: label{"k0", "v0"}, rawObject: `{"metadata": {"labels": {"k0": "v1"}}}`, allowed: false},
		"Allowed: object has required label":           {requiredLabel: label{"k0", "v0"}, rawObject: `{"metadata": {"labels": {"k0": "v0"}}}`, allowed: true},
		"Allowed: object has different required label": {requiredLabel: label{"k1", "v1"}, rawObject: `{"metadata": {"labels": {"k1": "v1"}}}`, allowed: true},
	}
	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			ts := httptest.NewServer(httpServer(tc.requiredLabel).Handler)
			defer ts.Close()
			ar := admissionv1.AdmissionReview{
				TypeMeta: metav1.TypeMeta{
					Kind:       "AdmissionReview",
					APIVersion: "admission.k8s.io/v1",
				},
				Request: &admissionv1.AdmissionRequest{
					Object: runtime.RawExtension{
						Raw: []byte(tc.rawObject),
					},
				},
			}
			arBytes, err := json.Marshal(ar)
			if err != nil {
				t.Error(err)
			}
			arString := string(arBytes)
			postBody := strings.NewReader(arString)
			res, err := http.Post(ts.URL, "application/json", postBody)
			if err != nil {
				t.Error(err)
			}
			resBody, err := ioutil.ReadAll(res.Body)
			_ = res.Body.Close()
			if err != nil {
				t.Error(err)
			}
			got := admissionv1.AdmissionReview{}
			if _, _, err = codecs.UniversalDeserializer().Decode(resBody, nil, &got); err != nil {
				t.Fatal(err)
			}
			if got.Response == nil {
				t.Fatal("nil response")
			}
			if got.Response.Allowed != tc.allowed {
				if tc.allowed {
					t.Errorf("wanted %v to be allowed", tc.rawObject)
				} else {
					t.Errorf("wanted %v to be disallowed", tc.rawObject)
				}
			}
			if got.TypeMeta != ar.TypeMeta {
				t.Errorf("wanted request TypeMeta %v to match response %v", ar.TypeMeta, got.TypeMeta)
			}
			if got.Response.UID != ar.Request.UID {
				t.Errorf("wanted request UID %v to match response %v", ar.Request.UID, got.Response.UID)
			}
		})
	}
}
