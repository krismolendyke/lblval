package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"encoding/json"

	"crypto/tls"

	admissionv1 "k8s.io/api/admission/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
)

var (
	scheme = runtime.NewScheme()
	codecs = serializer.NewCodecFactory(scheme)

	certFile = flag.String("certFile", "", "Path to certificate PEM file")
	keyFile  = flag.String("keyFile", "", "Path to private key PEM file")

	key = flag.String("key", "k", "Required label key")
	val = flag.String("val", "v", "Required label value")
)

func init() {
	utilruntime.Must(admissionv1.AddToScheme(scheme))
}

type label struct {
	k string
	v string
}

func (l *label) Valid() bool {
	if l.k != "" && l.v != "" {
		return true
	}
	return false
}

// LabelValidationServer is a k8s validating webhook server that only allows objects with a valid label to be admitted.
type LabelValidationServer struct {
	RequiredLabel label
}

func (s *LabelValidationServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("ServeHTTP: %v\n", r)

	// TODO validate Content-Type

	var body []byte
	if r.Body != nil {
		if data, err := ioutil.ReadAll(r.Body); err == nil {
			body = data
		}
	}

	req := admissionv1.AdmissionReview{}
	res := admissionv1.AdmissionReview{}
	if _, _, err := codecs.UniversalDeserializer().Decode(body, nil, &req); err != nil {
		log.Printf("Error decoding %v", err)
		res.Response = &admissionv1.AdmissionResponse{
			Result: &metav1.Status{
				Message: err.Error(),
			},
		}
	} else {
		res.Response = validateLabel(req, s.RequiredLabel)
	}

	res.TypeMeta = req.TypeMeta
	res.Response.UID = req.Request.UID

	log.Printf("admissionv1.AdmissionReview response: %v", res)

	resBytes, err := json.Marshal(res)
	if err != nil {
		log.Println(err)
	}
	if _, err = w.Write(resBytes); err != nil {
		log.Println(err)
	}
}

func validateLabel(ar admissionv1.AdmissionReview, l label) *admissionv1.AdmissionResponse {
	if !l.Valid() {
		return &admissionv1.AdmissionResponse{
			Result: &metav1.Status{
				Message: fmt.Sprintf("required label '%v' is invalid", l),
			},
		}
	}
	obj := struct {
		metav1.ObjectMeta `json:"metadata,omitempty"`
	}{}
	err := json.Unmarshal(ar.Request.Object.Raw, &obj)
	if err != nil {
		return &admissionv1.AdmissionResponse{
			Result: &metav1.Status{
				Message: err.Error(),
			},
		}
	}
	if len(obj.Labels) == 0 {
		return &admissionv1.AdmissionResponse{
			Result: &metav1.Status{
				Message: "object has no labels",
			},
		}
	}
	objV, ok := obj.Labels[l.k]
	if !ok {
		return &admissionv1.AdmissionResponse{
			Result: &metav1.Status{
				Message: fmt.Sprintf("object is missing required label '%v'", l.k),
			},
		}
	}
	if objV != l.v {
		return &admissionv1.AdmissionResponse{
			Result: &metav1.Status{
				Message: fmt.Sprintf("object labeled '%v: %v' is not allowed, must be labeled '%v: %v'", l.k, objV, l.k, l.v),
			},
		}
	}
	return &admissionv1.AdmissionResponse{
		Allowed: true,
		Result: &metav1.Status{
			Message: fmt.Sprintf("object labeled '%v: %v' is allowed", l.k, l.v),
		},
	}
}

func httpServer(l label) *http.Server {
	return &http.Server{Addr: ":8080", Handler: &LabelValidationServer{RequiredLabel: l}}
}

func main() {
	flag.Parse()

	s := httpServer(label{*key, *val})

	if *certFile != "" && *keyFile != "" {
		cert, err := tls.LoadX509KeyPair(*certFile, *keyFile)
		if err != nil {
			log.Fatal(err)
		}
		s.TLSConfig = &tls.Config{Certificates: []tls.Certificate{cert}}
		s.Addr = ":8443"
		log.Println("Serving HTTPS")
		log.Fatal(s.ListenAndServeTLS("", ""))
	} else {
		log.Println("Serving HTTP")
		log.Fatal(s.ListenAndServe())
	}
}
